import store from '../store/index.js';
const request = (options) => {
	const {
		url,
		data
	} = options
	
	const dataObj = {
		user_id: store.state.userInfo._id,
		...data
	}
	// 显示加载
	uni.showLoading({
		title: '加载中···'
	})
	// 返回一个Promise对象
	return new Promise((resolve,reject) => {
		// 使用云函数发起请求
		uniCloud.callFunction({
			name: url,
			data: dataObj
		}).then(res => {
			// 判断状态码
			if(res.result.status !== 200) {
				return uni.showToast({
					title: '获取数据失败',
					icon: 'none'
				})
			}
			// 隐藏加载
			uni.hideLoading()
			resolve(res.result)
		}).catch(err => {
			// 隐藏加载
			uni.hideLoading()
			reject(err)
		})
	})
}

export default request