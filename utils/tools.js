/* 
		时间格式化
	*/
	export const dataFormat = (originVal) => {

		if (typeof originVal === 'string') {
			originVal = parseInt(originVal)
		}

		const dt = new Date(originVal)

		// 不足两位的时候以0在开始位置补充   （padEnd在结束位置）
		const y = dt.getFullYear()
		const m = (dt.getMonth() + 1 + '').padStart(2, '0')
		const d = (dt.getDate() + '').padStart(2, '0')

		const hh = (dt.getHours() + '').padStart(2, '0')
		const mm = (dt.getMinutes() + '').padStart(2, '0')
		const ss = (dt.getSeconds() + '').padStart(2, '0')

		return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
	}

	/*
		文件随机名称
	*/
	export const lorem = (length) => {
		length = length || 32
		var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678' /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
		var maxPos = $chars.length
		var random = ''
		for (let i = 0; i < length; i++) {
			random += $chars.charAt(Math.floor(Math.random() * maxPos));
		}
		return random
	}

	/*
		获取文件类型
	*/
	export const getType = (file) => {
		var filename = file
		var index1 = filename.lastIndexOf(".")
		var index2 = filename.length
		var type = filename.substring(index1, index2)
		return type
	}
