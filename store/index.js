import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

 const store = new Vuex.Store({
	state: {
		// 用户信息
		userInfo: uni.getStorageSync('__userInfo') || {},
		// 历史记录
		historyList: uni.getStorageSync('__history') || []
	},
	mutations: {
		SET_USER_INFO(state, userInfo) {
			state.userInfo = userInfo
		},
		// 修改历史记录
		SET_HISTORY(state, history) {
			state.historyList = history
		},
		// 清除历史记录
		CLEAR_HISTORY(state) {
			state.historyList = []
		}
	},
	actions: {
		// 异步调用SET_USER_INFO方法修改 state 的值
		set_userInfo(content, userInfo) {
			uni.setStorageSync('__userInfo', userInfo)
			content.commit('SET_USER_INFO', userInfo)
		},
		// 异步调用SET_HISTORY方法修改 state 的值
		set_history(content, history) {
			// 拿到原来的数据
			let list = content.state.historyList
			// 向原来的数据前面添加一个新的记录
			list.unshift(history)
			uni.setStorageSync('__history', list)
			// 调用SET_HISTORY方法给historyList赋值
			content.commit('SET_HISTORY', list)
		},
		// 异步调用SET_HISTORY方法修改 state 的值
		clear_history(content) {
			// 清除本地存储
			uni.removeStorageSync('__history')
			// 调用CLEAR_HISTORY方法清空历史记录
			content.commit('CLEAR_HISTORY')
		}
		
	},
	modules: {},
	getters: {}
})

export default store