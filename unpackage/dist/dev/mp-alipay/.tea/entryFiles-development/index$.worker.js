if(!self.__appxInited) {
self.__appxInited = 1;


require('./config$');


      function getUserAgentInPlatformWeb() {
        return typeof navigator !== 'undefined' ? navigator.swuserAgent || navigator.userAgent || '' : '';
      }
      if(getUserAgentInPlatformWeb() && (getUserAgentInPlatformWeb().indexOf('LyraVM') > 0 || getUserAgentInPlatformWeb().indexOf('AlipayIDE') > 0) ) {
        var AFAppX = self.AFAppX.getAppContext ? self.AFAppX.getAppContext().AFAppX : self.AFAppX;
      } else {
        importScripts('https://appx/af-appx.worker.min.js');
        var AFAppX = self.AFAppX;
      }
      self.getCurrentPages = AFAppX.getCurrentPages;
      self.getApp = AFAppX.getApp;
      self.Page = AFAppX.Page;
      self.App = AFAppX.App;
      self.my = AFAppX.bridge || AFAppX.abridge;
      self.abridge = self.my;
      self.Component = AFAppX.WorkerComponent || function(){};
      self.$global = AFAppX.$global;
      self.requirePlugin = AFAppX.requirePlugin;
    

if(AFAppX.registerApp) {
  AFAppX.registerApp({
    appJSON: appXAppJson,
  });
}



function success() {
require('../../app');
require('../../components/uni-icons/uni-icons?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/navbar/navbar?hash=4f3f44db986be84bb3e7c699e0f2fdbffe19a890');
require('../../components/tab/tab?hash=4f3f44db986be84bb3e7c699e0f2fdbffe19a890');
require('../../components/list-scroll/list-scroll?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/likes/likes?hash=4f3f44db986be84bb3e7c699e0f2fdbffe19a890');
require('../../components/list-card/list-card?hash=e0f2a6dc341f22d810f5b93bc1eb28d2ba77e03a');
require('../../components/uni-load-more/uni-load-more?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/list/list-item?hash=34d567f05cd625162b1dad08e8f1af1c125ee53c');
require('../../components/list/list?hash=97a3f0b49d18befee70a599d5f62115682226a62');
require('../../components/list-author/list-author?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/comment-box/comment-box?hash=156e0d11c94c3438bf40fd4636c8edc125f83850');
require('../../components/uni-transition/uni-transition?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/uni-popup/uni-popup?hash=63f3d097e78ac04213190ce4a92c81860617f7b4');
require('../../components/feng-parse/components/wxParseImg?hash=591ac642b6f57a184fac9330fa292ccf33c32471');
require('../../components/feng-parse/components/wxParseVideo?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/feng-parse/components/wxParseAudio?hash=591ac642b6f57a184fac9330fa292ccf33c32471');
require('../../components/feng-parse/components/wxParseTable?hash=ec7c5687c7f2ffa836caf3a93e877914426baff6');
require('../../components/feng-parse/components/wxParseTemplate11?hash=d82954bcfebf7e7f889f60f993d0080e00fcbb2f');
require('../../components/feng-parse/components/wxParseTemplate10?hash=f0a4a394dbb8901e4e1558b90c2a1198671acb1e');
require('../../components/feng-parse/components/wxParseTemplate9?hash=96c80a475892bef56029c77ec13307188c5a1aa1');
require('../../components/feng-parse/components/wxParseTemplate8?hash=323b58ca0c30aaac384e049b3b191f97c5d9cc53');
require('../../components/feng-parse/components/wxParseTemplate7?hash=2afc6c985df675db84aab9b5af51de0507c5f97c');
require('../../components/feng-parse/components/wxParseTemplate6?hash=1519e74fe522e2ecf8866acade6e183a534eaba1');
require('../../components/feng-parse/components/wxParseTemplate5?hash=bf80b577c5d8de32d7d4af2f460db17af252a906');
require('../../components/feng-parse/components/wxParseTemplate4?hash=cb37ba4284d6db3473487bb4746561fdef01ec2d');
require('../../components/feng-parse/components/wxParseTemplate3?hash=cf15b32001216dbbfc4a74bf3037885e8494ead0');
require('../../components/feng-parse/components/wxParseTemplate2?hash=e94d8ee34941fd54f6af4693719a616c03048f1e');
require('../../components/feng-parse/components/wxParseTemplate1?hash=a9161a893b7d050930c51f0451d19bd6b904372a');
require('../../components/feng-parse/components/wxParseTemplate0?hash=90a611a53ef0413e4016d3615336e7f1dddcfc1c');
require('../../components/feng-parse/parse?hash=93b473c725915806ae3073b33b61bf121560d593');
require('../../pages/tabbar/index/index?hash=d593c0804300b2ff6280a20e9008c0df4e97c9a8');
require('../../pages/tabbar/my/my?hash=af30089a14de5bba22f51cbb6b3d48bc536087f6');
require('../../pages/tabbar/follw/follw?hash=4fcab1444de769cf6db1b26a68dd384bbfab5ea8');
require('../../pages/home-search/home-search?hash=55deaa0b330c49593c7d18dc1ff8a70f471d83b7');
require('../../pages/home-label/home-label?hash=af30089a14de5bba22f51cbb6b3d48bc536087f6');
require('../../pages/home-detail/home-detail?hash=b5414f8e146a236306f05c6edeef84fb20787dfc');
require('../../pages/detail-comments/detail-comments?hash=ab40c7bfc2a286e5846ffe8c5ca09daa955a0a48');
require('../../pages/my-article/my-article?hash=46182d723100061004ed14c9005659d32b5c21f6');
require('../../pages/feedback/feedback?hash=af30089a14de5bba22f51cbb6b3d48bc536087f6');
}
self.bootstrapApp ? self.bootstrapApp({ success }) : success();
}