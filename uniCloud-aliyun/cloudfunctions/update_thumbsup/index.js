'use strict';
// 获取数据库引用
const db = uniCloud.database()
const dbCmd = db.command
exports.main = async (event, context) => {
	const {
		user_id,
		article_id
	} = event
	
	// 根据用户id查找
	const userinfo = await db.collection('user').doc(user_id).get()
	
	const thumbs_up_article_ids = userinfo.data[0].thumbs_up_article_ids
	
	let thumbs_ids = null 
	
	// 判断是否包含文章id
	if (thumbs_up_article_ids.includes(article_id)) {
		return {
			status: 200,
			msg: '您已经点过赞了'
		}
	} else {
		thumbs_ids = dbCmd.addToSet(article_id)
	}
	
	// 更新数据库
	await db.collection('user').doc(user_id).update({
		thumbs_up_article_ids: thumbs_ids
	})
	
	// 更新数据库
	await db.collection('article').doc(article_id).update({
		// inc 原子操作，可以减少一次请求
		thumbs_up_count: dbCmd.inc(1)
	})
	
	
	//返回数据给客户端
	return {
		status: 200,
		msg: '点赞成功'
	}
};
