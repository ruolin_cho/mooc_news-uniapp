'use strict';
const db = uniCloud.database()

exports.main = async (event, context) => {
	const {
		user_id = "5ff82c2f5c2b5100018f487a"
	} = event
	
	// 判断是否有登录
	if (!user_id) {
		return {
			status: 201,
			msg: '获取用户信息失败'
		}
	}
	
	const userinfo = await db.collection('user').doc(user_id).get()
	
	//返回数据给客户端
	return {
		status: 200,
		msg: '获取成功',
		data: userinfo.data[0]
	}
};
