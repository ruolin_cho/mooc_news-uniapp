'use strict';
// 获取数据库引用
const db = uniCloud.database()
const $ = db.command.aggregate
exports.main = async (event, context) => {
	const {
		user_id,
		pageSize = 10,
		page = 1
	} = event
	
	let userinfo = await db.collection('user').doc(user_id).get()
	userinfo = userinfo.data[0]
	
	const lists = await db.collection('user')
	.aggregate()
	.addFields({
		is_like: $.in(['$id',userinfo.author_likes_ids])
	})
	.match({
		is_like: true
	})
	// 要跳过多少数据
	.skip(pageSize * (page - 1))
	// 显示多少条数据
	.limit(pageSize)
	.end()
	//返回数据给客户端
	return {
		status: 200,
		msg: '数据获取成功',
		data: lists.data
	}
};
