'use strict';
// 获取数据库引用
const db = uniCloud.database()

const $ = db.command.aggregate

exports.main = async (event, context) => {
	// default value
	const {
		user_id,
		query
	} = event
	
	const userinfo = await db.collection('user').doc(user_id).get()
	const article_likes_ids = userinfo.data[0].article_likes_ids
	
	let list = await db.collection('article')
	.aggregate()
	// 追加字段
	.addFields({
		is_like: $.in(['$_id', article_likes_ids])
	})
	.project({
		content: false
	})
	.match({
		title: new RegExp(query)
	})
	.end()
	
	// 返回数据给客户端
	return {
		status: 200,
		msg: '数据请求成功',
		data: list.data
	}

};
