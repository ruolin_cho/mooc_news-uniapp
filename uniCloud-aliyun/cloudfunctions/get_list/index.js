'use strict';
// 获取数据库引用
const db = uniCloud.database()

const $ = db.command.aggregate

exports.main = async (event, context) => {
	// default value
	const {
		user_id = "5ff82c2f5c2b5100018f487a",
		query = '全部',
		page = 1,
		pageSize = 10
	} = event
	
	let matchObj = {}
	
	if(query !== '全部') {
		matchObj = {
			classify: query
		}
	}
	
	const userinfo = await db.collection('user').doc(user_id).get()
	const article_likes_ids = userinfo.data[0].article_likes_ids
	
	let list = await db.collection('article')
	.aggregate()
	// 追加字段
	.addFields({
		is_like: $.in(['$_id', article_likes_ids])
	})
	.match(matchObj)
	.project({
		content: false
	})
	// 要跳过多少数据
	.skip(pageSize*(page-1))
	// 显示多少条数据
	.limit(pageSize)
	.end()
	
	// 返回数据给客户端
	return {
		status: 200,
		msg: '数据请求成功',
		data: list.data
	}

};
