'use strict';
// 获取数据库引用
const db = uniCloud.database()
const $ = db.command.aggregate
exports.main = async (event, context) => {
	const {
		user_id,
		type
	} = event
	
	// 根据条件过滤文档对象
	let matchObj = {}
	
	// 如果 type 不是为 all 执行过滤
	if(type !== 'all') {
		matchObj = {
			current: true
		}
	}
	
	// 获取指定用户表
	let userinfo = await db.collection('user').doc(user_id).get()
	
	userinfo = userinfo.data[0]
	
	let label = await db.collection('label')
	.aggregate()
	// 添加 current 字段
	.addFields({
		current: $.in(['$_id', $.ifNull([userinfo.label_ids, []])])
	})
	// 根据条件过滤文档，并且把符合条件的文档传递给下一个流水线阶段
	.match(matchObj)
	.end()
	
	//返回数据给客户端
	return {
		status: 200,
		msg: '数据请求获取成功',
		data: label.data
	}
};
