'use strict';
// 获取数据库引用
const db = uniCloud.database()
// 聚合操作符
const $ = db.command.aggregate
	exports.main = async (event, context) => {
		const {
			user_id,
			pageSize = 5,
			page = 1
		} = event

		let userinfo = await db.collection('user').doc(user_id).get()
		userinfo = userinfo.data[0]

		const lists = await db.collection('article')
			.aggregate()
			.addFields({
				is_like: $.in(['$_id', userinfo.article_likes_ids])
			})
			.project({
				content: false
			})
			.match({
				is_like: true
			})
			// 要跳过多少数据
			.skip(pageSize * (page - 1))
			// 显示多少条数据
			.limit(pageSize)
			.end()
		//返回数据给客户端
		return {
			status: 200,
			msg: '更新成功',
			data: lists.data
		}
	};
