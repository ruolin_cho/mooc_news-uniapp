'use strict';
const db = uniCloud.database()
const $ = db.command.aggregate
exports.main = async (event, context) => {
	const {
		user_id,  // 用户id
		article_id, // 文章id
		pageSize = 10,
		page = 1
	} = event
	
	const list = await db.collection('article')
	.aggregate()
	.match({
		_id: article_id
	})
	.unwind('$comments')
	.project({
		_id: false,
		comments: true
	})
	.replaceRoot({
		newRoot: '$comments'
	})
	// 要跳过多少数据
	.skip(pageSize * (page - 1))
	// 显示多少条数据
	.limit(pageSize)
	.end()
	//返回数据给客户端
	return {
		status: 200,
		msg: '请求成功',
		data: list.data
	}
};
