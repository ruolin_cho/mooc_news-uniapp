'use strict';
// 获取数据库引用
const db = uniCloud.database()
const dbCmd = db.command
exports.main = async (event, context) => {
	const {
		user_id,
		author_id
	} = event
	
	// 获取用户信息
	const user = await db.collection('user').doc(user_id).get()
	// 获取用户关注作者的列表
	const author_likes_ids = user.data[0].author_likes_ids
	
	let author_ids = null
	// 判断用户是否已关注
	if (author_likes_ids.includes(author_id)) {
		author_ids = dbCmd.pull(author_id)
	} else {
		author_ids = dbCmd.addToSet(author_id)
	}
	
	await db.collection('user').doc(user_id).update({
		author_likes_ids: author_ids
	})
	
	//返回数据给客户端
	return {
		status: 200,
		msg: '更新成功'
	}
};
