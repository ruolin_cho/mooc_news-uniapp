'use strict';
// 获取数据库引用
const db = uniCloud.database()
const dbCmd = db.command
exports.main = async (event, context) => {
	const {
		user_id,
		article_id
	} = event

	// 根据用户id查找
	const userinfo = await db.collection('user').doc(user_id).get()

	const article_id_ids = userinfo.data[0].article_likes_ids

	let dbCmdFuns = null

	// 判断是否包含文章id
	if (article_id_ids.includes(article_id)) {
		dbCmdFuns = dbCmd.pull(article_id)
	} else {
		dbCmdFuns = dbCmd.addToSet(article_id)
	}

	// 更新数据库
	await db.collection('user').doc(user_id).update({
		article_likes_ids: dbCmdFuns
	})

	return {
		status: 200,
		msg: '请求成功',
		data: userinfo.data[0]
	}
};
