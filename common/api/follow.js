import request from '@/utils/request.js'

/* 
	获取关注的文章
 */
export const get_follow = (data) => {
	return request({
		url: 'get_follow',
		data: data 
	})
}

/* 
	获取关注的作者
 */
export const get_author = (data) => {
	return request({
		url: 'get_author',
		data: data 
	})
}
