import request from '@/utils/request.js'

/* 
	获取tab的label
 */

export const get_label = (data) => {
	return request({
		url: 'get_label',
		data
	})
}

/* 
	更新标签
 */

export const update_label = (data) => {
	return request({
		url: 'update_label',
		data
	})
}


/* 
	获取文章不包含内容
 */

export const get_list = (data) => {
	return request({
		url: 'get_list',
		data: data 
	})
}

/* 
	收藏文章
 */

export const update_like = (data) => {
	return request({
		url: 'update_like',
		data: data 
	})
}

/* 
	文章搜索
 */

export const get_search = (data) => {
	return request({
		url: 'get_search',
		data: data 
	})
}

/* 
	获取指定文章
 */

export const get_detail = (data) => {
	return request({
		url: 'get_detail',
		data: data 
	})
}

/* 
	评论文章
 */
export const update_comment = (data) => {
	return request({
		url: 'update_comment',
		data: data 
	})
}

/* 
	获取评论
 */
export const get_comments = (data) => {
	return request({
		url: 'get_comments',
		data: data 
	})
}

/* 
	关注作者
 */
export const update_author = (data) => {
	return request({
		url: 'update_author',
		data: data 
	})
}

/* 
	点赞
 */
export const update_thumbsup = (data) => {
	return request({
		url: 'update_thumbsup',
		data: data 
	})
}
