import request from '@/utils/request.js'

/* 
	获取用户信息
 */
export const get_user = (data) => {
	return request({
		url: 'get_user',
		data
	})
}

/* 
	获取我的文章
 */
export const get_my_article = (data) => {
	return request({
		url: 'get_my_article',
		data
	})
}

/* 
	提交反馈意见 
 */
export const update_feedback = (data) => {
	return request({
		url: 'update_feedback',
		data
	})
}